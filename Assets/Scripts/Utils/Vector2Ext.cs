﻿using UnityEngine;

namespace Utils
{
    public static class Vector2Ext
    {
        public static Vector3 ToX0Y(this UnityEngine.Vector2 vector2)
        {
            return new Vector3(vector2.x,0,vector2.y);
        }
    }
}