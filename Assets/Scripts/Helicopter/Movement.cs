﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

namespace Helicopter
{
    [RequireComponent(typeof(Rigidbody))]
    public class Movement : MonoBehaviour
    {
        private Rigidbody _rb;
        private Vector2 _axis = Vector2.zero;
        private Vector3 _lastVelocity;
        public float speedMultiplier = 2f;
        public float rotationFactor = 2f;
        public Transform model;
        public Transform targetRotate;
        private void Start()
        {
            _rb = GetComponent<Rigidbody>();
        }

        private float Acceleration => _rb.velocity.sqrMagnitude - _lastVelocity.sqrMagnitude;
        
        
        public void OnMove(InputAction.CallbackContext ctx)
        {
            _axis = ctx.ReadValue<Vector2>();
           
        }
        private void FixedUpdate()
        {
            Vector3 force = _axis.ToX0Y().normalized * speedMultiplier;
            _rb.AddForce(force, ForceMode.Force);
            _lastVelocity = _rb.velocity;
        }

        private void Update()
        {
            if (_axis.ToX0Y().magnitude > 0.5f)
                targetRotate.position = model.position + _axis.ToX0Y() * 3;
           
            Vector3 targetDirection = (targetRotate.position - model.position).normalized;

            model.rotation = Quaternion.RotateTowards(model.rotation, Quaternion.LookRotation(targetDirection),
                Quaternion.Angle(model.rotation, Quaternion.LookRotation(targetDirection)) * Time.deltaTime * rotationFactor);

        }
    }
}
